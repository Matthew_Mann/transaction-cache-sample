﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransactionSample.Cache;
using TransactionSample.Data;

namespace TransactionSampleTests
{
    [TestClass]
    public class TransactionDictionaryTests
    {
        private IDictionary<string,ICacheObject> _objectCache;


        [TestInitialize]
        public void TestInit()
        {
            _objectCache = new Dictionary<string, ICacheObject>
            {
                {"obj1", new DataObject(1, "1", DateTime.Now)},
                {"obj2", new DataObject(2, "2", DateTime.Now)},
                {"obj3", new DataObject(3, "3", DateTime.Now)},
                {"obj4", new DataObject(4, "4", DateTime.Now)},
                {"obj5", new DataObject(5, "5", DateTime.Now)}
            };
        }

        [TestMethod]
        public void CacheCommitSuccess()
        {
            using (var scope = new TransactionScope())
            {
                var transCache = new TransactionDictionary<string, ICacheObject>(_objectCache)
                {
                    {"obj6", new DataObject(99, "I should be here", new DateTime(2001, 1, 15))}
                };

                scope.Complete();
            }

            Assert.AreEqual("I should be here",_objectCache["obj6"].Str);
            Assert.AreEqual(99, _objectCache["obj6"].Integer);
        }

        [TestMethod]
        public void CacheCommitFail()
        {
            using (var scope = new TransactionScope())
            {
                var transCache = new TransactionDictionary<string, ICacheObject>(_objectCache)
                {
                    {"obj6", new DataObject(99, "I should be here", new DateTime(2001, 1, 15))}
                };
            }

            Assert.AreEqual(5,_objectCache.Count);            
        }

        [TestMethod]
        public void CacheEditSuccess()
        {
            using (var scope = new TransactionScope())
            {
                var transCache = new TransactionDictionary<string, ICacheObject>(_objectCache);

                transCache["obj3"] = new DataObject(99,"I am taking over",DateTime.Now);

                scope.Complete();
            }

            Assert.AreEqual("I am taking over", _objectCache["obj3"].Str);
            Assert.AreEqual(99, _objectCache["obj3"].Integer);
        }

        [TestMethod]
        public void CacheEditFail()
        {
            using (var scope = new TransactionScope())
            {
                var transCache = new TransactionDictionary<string, ICacheObject>(_objectCache);

                transCache["obj3"] = new DataObject(99, "I am taking over", DateTime.Now);                
            }

            Assert.AreEqual("3", _objectCache["obj3"].Str);
            Assert.AreEqual(3, _objectCache["obj3"].Integer);
        }

        [TestMethod]
        public void CacheRemoveSuccess()
        {
            using (var scope = new TransactionScope())
            {
                var transCache = new TransactionDictionary<string, ICacheObject>(_objectCache);
                transCache.Remove("obj3");
                scope.Complete();
            }

            Assert.AreEqual(4, _objectCache.Count);
            Assert.IsFalse(_objectCache.ContainsKey("obj3"));
        }

        [TestMethod]
        public void CacheRemoveFail()
        {
            using (var scope = new TransactionScope())
            {
                var transCache = new TransactionDictionary<string, ICacheObject>(_objectCache);
                transCache.Remove("obj3");             
            }

            Assert.AreEqual(5, _objectCache.Count);
            Assert.IsTrue(_objectCache.ContainsKey("obj3"));
        }

        [TestMethod]
        public void ConcurrentCacheEditOneFail()
        {
            var transCache = new TransactionDictionary<string, ICacheObject>(_objectCache);
            Parallel.Invoke(
                () =>
            {
                using (var scope = new TransactionScope())
                {
                   

                    transCache["obj3"] = new DataObject(26, "I am Sparta", DateTime.MinValue);

                    scope.Complete();
                }
            },
             () =>
            {
                using (var scope = new TransactionScope())
                {                    
                    transCache["obj3"] = new DataObject(99, "I am taking over", DateTime.MaxValue);                   
                }
            }
            );

            Assert.AreEqual("I am Sparta", _objectCache["obj3"].Str);
            Assert.AreEqual(26, _objectCache["obj3"].Integer);
        }


        [TestCleanup]
        public void CleanupTest()
        {
            _objectCache.Clear();
        }
    }
}
