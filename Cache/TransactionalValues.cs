﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Transactions.Configuration;

namespace TransactionSample.Cache
{
    public class TransactionalValues<TKey,TValue> :IEnlistmentNotification, IDictionary<TKey,TValue> where TValue: ICloneable
    {       
        //underlying cache
        private IDictionary<TKey, TValue> _backingCache;
        //transaction cache.  
        private Dictionary<TKey, TValue> _transactionValues = new Dictionary<TKey, TValue>();
        //undo list to iterate through when a rollback is encountered.
        private List<KeyValuePair<TKey, TValue>> _undo = new List<KeyValuePair<TKey, TValue>>();
        //action to apply a lock on the current transaction
        private Action _lockTransaction;
        //unlock the current transaction from committing to the underlying cache
        private Action _unlockTransaction;

        //flags whether the thransaction has been properly prepared for a commit.
        private bool _isPrepared = false;
        
        /// <summary>
        /// The TransactionValues dictionary holds the cache objects that have been edited,added, or removed from the cache during the transaction
        /// </summary>
        public IDictionary<TKey, TValue> TransactionValues
        {            
            get
            {
                if(_transactionValues == null)
                    return new Dictionary<TKey, TValue>();

                return _transactionValues;                
            }
        }

        /// <summary>
        /// assigns the underlying cache as well as the cache lock and unlock actions.
        /// enlists in the current transaction.
        /// </summary>
        /// <param name="backingCache"></param>
        /// <param name="lockTransaction"></param>
        /// <param name="unlockTransaction"></param>
        public TransactionalValues(IDictionary<TKey, TValue> backingCache, Action lockTransaction, Action unlockTransaction )
        {           
            _backingCache = backingCache;
            if (Transaction.Current == null)
                throw new InvalidOperationException("Cannot use Transactional Cache without an open transaction");

            _lockTransaction = lockTransaction;
            _unlockTransaction = unlockTransaction;

            Transaction.Current.EnlistVolatile(this, EnlistmentOptions.None);
        }

        /// <summary>
        /// Updates/adds/removes the corresponding key value pair in the provided dictionary.
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="kvp"></param>
        public void UpdateWithPair(IDictionary<TKey, TValue> dictionary, KeyValuePair<TKey, TValue> kvp)
        {
            if (dictionary.ContainsKey(kvp.Key))
            {
                if (EqualityComparer<TValue>.Default.Equals(default(TValue),kvp.Value))
                    dictionary.Remove(kvp.Key);
                else
                    dictionary[kvp.Key] = kvp.Value;
            }
            else
                dictionary.Add(kvp);
        }

        /// <summary>
        /// Updates the provided dictionary with with values from the transaction values dictionary.  Adds these values to the undo list.
        /// </summary>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        public List<KeyValuePair<TKey, TValue>> Update(IDictionary<TKey, TValue> dictionary)
        {
            var undoList = new List<KeyValuePair<TKey, TValue>>();
            foreach (var kvp in _transactionValues)
            {
                TValue oldValue;
                var has = dictionary.TryGetValue(kvp.Key, out oldValue);
                UpdateWithPair(dictionary, kvp);
                if (has)
                    undoList.Add(new KeyValuePair<TKey, TValue>(kvp.Key, oldValue));
                else
                    undoList.Add(new KeyValuePair<TKey, TValue>(kvp.Key, default(TValue)));
            }
            return undoList;
        }

   
        /// <summary>
        /// prepares the transaction to be committed.  
        /// this is where the transaction Values are moved to the underlying cache array.
        /// the undo list is populated incase a rollback is encountered.
        /// </summary>
        private void Prep()
        {
            _lockTransaction();
            _undo = Update(_backingCache);
            _isPrepared = true;
        }
        
        /// <summary>
        /// Gets the current state of the cache with all current transaction operations applied
        /// </summary>
        /// <returns></returns>
        public IDictionary<TKey,TValue> Current()
        {
            var cacheCopy = _backingCache.ToDictionary(entry => entry.Key, entry => (TValue) entry.Value.Clone());
            Update(_backingCache);
            return cacheCopy;
        }

       
        /// <summary>
        /// gets the value which matches the given key.  
        /// First checks the transactionalValues and returns the associated value from there if it is found
        /// else it gets it from the underlying cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue Get(TKey key)
        {
            var keyValue = default(TValue);
            var tranHas = _transactionValues.TryGetValue(key, out keyValue);
            if (tranHas)
                return _transactionValues[key];

            var backCacheHas = _backingCache.TryGetValue(key, out keyValue);
            if (!backCacheHas)
                return default(TValue);
            return keyValue;
        }
      
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return Current().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
      
        public void Add(KeyValuePair<TKey, TValue> item)
        {
           if(_transactionValues.ContainsKey(item.Key))
               throw new Exception("Key already exists");

            _transactionValues.Add(item.Key, item.Value);
        }

        public void Clear()
        {
            _transactionValues.Clear();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            var value = Get(item.Key);
            return item.Value.Equals(value);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }
       
        /// <summary>
        /// Sets the item for the key value pair in the transactionValues array to the Types default value so it can be removed during a commit.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            var value = Get(item.Key);
            if (value.Equals(item.Value))
            {
                _transactionValues[item.Key] = default(TValue);
                return true;
            }
            return false;
        }

        public int Count {
            get { return Current().Count; }
        }
        public bool IsReadOnly { get; private set; }
        public bool ContainsKey(TKey key)
        {
            if (!EqualityComparer<TValue>.Default.Equals(default(TValue), Get(key)))
                return true;

            return false;
        }

        public void Add(TKey key, TValue value)
        {
            _transactionValues.Add(key,value);
        }

        /// <summary>
        /// Sets the item for the key in the transactionValues array to the Types default value so it can be removed during a commit.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Remove(TKey key)
        {
            _transactionValues[key] = default(TValue);
            return true;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            if (_transactionValues.ContainsKey(key))
            {
                value = default(TValue);
                return false;
            }
            value = _transactionValues[key];
            return true;
        }

        public TValue this[TKey key]
        {
            get { return Get(key); }
            set
            {
                if (_transactionValues.ContainsKey(key))
                    _transactionValues[key] = value;
                else
                {
                    _transactionValues.Add(key,value);
                }
            }
        }

        public ICollection<TKey> Keys { get; private set; }
        public ICollection<TValue> Values { get; private set; }

        #region IEnlistment

        /// <summary>
        /// Prepares the transaction to be committed.
        /// This is where the underlying cache array is updated with the transaction values
        /// an undo list is build to undo if a rollback is encountered.
        /// </summary>
        /// <param name="preparingEnlistment"></param>
        public void Prepare(PreparingEnlistment preparingEnlistment)
        {
            try
            {
                Prep();
                preparingEnlistment.Prepared();
            }
            catch (Exception ex)
            {
                preparingEnlistment.ForceRollback(ex);
            }            
        }


        /// <summary>
        /// checks if the transaction has been prepared
        /// if the transaction has not been prepared (used in a 2 phase commit) then prepare the transaction before unlocking
        /// </summary>
        /// <param name="enlistment"></param>
        public void Commit(Enlistment enlistment)
        {
            if (!_isPrepared)
                Prep();
            _unlockTransaction();
            enlistment.Done();
        }

        /// <summary>
        /// rolls back the underlying cache using the undo list created during the prepare step
        /// </summary>
        /// <param name="enlistment"></param>
        public void Rollback(Enlistment enlistment)
        {
            foreach (var kvp in _undo)
            {
                UpdateWithPair(_backingCache,kvp);
            }
            _undo.Clear();
            _unlockTransaction();
        }

        public void InDoubt(Enlistment enlistment)
        {
            enlistment.Done();
        }

        #endregion
    }
}
