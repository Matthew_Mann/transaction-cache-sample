﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionSample.Cache
{
    public interface ICacheObject : ICloneable
    {
        int Integer { get; set; }
        string Str { get; set; }
        DateTime Date { get; set; }
    }
}
