﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace TransactionSample.Cache
{
    /*
     *Taken from Juval Lowy's Volatile Resource Managers in .NET
     * http://msdn.microsoft.com/en-us/magazine/cc163688.aspx#S5     
     */
    public class TransactionLock
    {
        private LinkedList<KeyValuePair<Transaction, ManualResetEvent>> m_PendingTransactions =
            new LinkedList<KeyValuePair<Transaction, ManualResetEvent>>();

        private Transaction m_OwningTransaction;
        // Property provides thread-safe access to m_OwningTransaction 
        private Transaction OwningTransaction
        {
            get { return m_OwningTransaction; }
            set { m_OwningTransaction = value; }
        }

        public bool Locked
        {
            get { return OwningTransaction != null; }
        }

        public void Lock()
        {
            Lock(Transaction.Current);
        }

        public void Lock(Transaction transaction)
        {
            Monitor.Enter(this);
            if (OwningTransaction == null)
            {
                //Acquire the transaction lock 
                if (transaction != null)
                    OwningTransaction = transaction;
                Monitor.Exit(this);
                return;
            }
            else 
                //Some transaction owns the lock 
            {
                //We're done if it's the same one as the method parameter 
                if (OwningTransaction == transaction)
                {
                    Monitor.Exit(this);
                    return;
                } 
                    //Otherwise, need to acquire the transaction lock 
                else
                {
                    ManualResetEvent manualEvent = new ManualResetEvent(false);
                    KeyValuePair<Transaction, ManualResetEvent> pair =
                        new KeyValuePair<Transaction, ManualResetEvent>(transaction, manualEvent);
                    m_PendingTransactions.AddLast(pair);
                    if (transaction != null)
                    {
                        transaction.TransactionCompleted += delegate
                        {
                            lock (this)
                            {
                                //Pair may have already been removed if unlocked 
                                m_PendingTransactions.Remove(pair);
                            }
                            lock (manualEvent)
                            {
                                if (!manualEvent.SafeWaitHandle.IsClosed)
                                {
                                    manualEvent.Set();
                                }
                            }
                        };
                    }
                    Monitor.Exit(this);
                    //Block the transaction or the calling thread 
                    manualEvent.WaitOne();
                    lock (manualEvent) manualEvent.Close();
                }
            }
        }

        public void Unlock()
        {           
            lock (this)
            {
                OwningTransaction = null;
                LinkedListNode<KeyValuePair<Transaction, ManualResetEvent>> node = null;
                if (m_PendingTransactions.Count > 0)
                {
                    node = m_PendingTransactions.First;
                    m_PendingTransactions.RemoveFirst();
                }
                if (node != null)
                {
                    Transaction transaction = node.Value.Key;
                    ManualResetEvent manualEvent = node.Value.Value;
                    Lock(transaction);
                    lock (manualEvent)
                    {
                        if (!manualEvent.SafeWaitHandle.IsClosed)
                        {
                            manualEvent.Set();
                        }
                    }
                }
            }
        }
    }
}
