﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace TransactionSample.Cache
{
    public class TransactionDictionary<TKey, TValue>: IDictionary<TKey,TValue> where TValue : ICloneable
    {
        //underlying cache
        private IDictionary<TKey, TValue> _backingCache;
        //dictionary of transactions and their associated transaction cache
        private Dictionary<Transaction, IDictionary<TKey, TValue>> _transactions = new Dictionary<Transaction, IDictionary<TKey, TValue>>(); 
        object _sync = new object();
        //locking class to ensure only 1 transaction is updating underlying cache at any given time
        private TransactionLock _transactionLock = new TransactionLock();

        public TransactionDictionary(IDictionary<TKey, TValue> backingCache)
        {
            _backingCache = backingCache;
        }

        /// <summary>
        /// Gets the transaction cache values for the given transactions.
        /// if no transaction cache exists for the current transaction then one is created.
        /// </summary>
        /// <param name="txn"></param>
        /// <returns></returns>
        public IDictionary<TKey, TValue> GetValues(Transaction txn)
        {
            if (txn == null)
                return _backingCache;

            IDictionary<TKey, TValue> txnValues;
            lock (_sync)
            {                   
                var found = _transactions.TryGetValue(txn, out txnValues);
                if (!found)
                    txnValues = new TransactionalValues<TKey, TValue>(_backingCache,
                    () => _transactionLock.Lock(txn), () =>
                    {
                        _transactionLock.Unlock();
                        lock (_sync)
                        {
                            _transactions.Remove(txn);
                        }
                    });

                _transactions[txn] = txnValues;
            }
            return txnValues;
        }

        public IDictionary<TKey, TValue> GetTransactionValues()
        {
            return GetValues(Transaction.Current);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return GetTransactionValues().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (GetTransactionValues() as IEnumerable).GetEnumerator();
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            GetTransactionValues().Add(item);
        }

        public void Clear()
        {
            GetTransactionValues().Clear();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return GetTransactionValues().Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return GetTransactionValues().Remove(item);
        }

        public int Count { get { return GetTransactionValues().Count; } }
        public bool IsReadOnly { get { return false; } }
        public bool ContainsKey(TKey key)
        {
            return GetTransactionValues().ContainsKey(key);
        }

        public void Add(TKey key, TValue value)
        {
            GetTransactionValues().Add(key,value);
        }

        public bool Remove(TKey key)
        {
            return GetTransactionValues().Remove(key);
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return GetTransactionValues().TryGetValue(key, out value);
        }

        public TValue this[TKey key]
        {
            get { return GetTransactionValues()[key]; }
            set { GetTransactionValues()[key] = value; }
        }

        public ICollection<TKey> Keys { get { return GetTransactionValues().Keys; } }
        public ICollection<TValue> Values { get { return GetTransactionValues().Values; } }
    }
}
