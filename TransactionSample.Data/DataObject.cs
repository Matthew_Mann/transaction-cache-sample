﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransactionSample.Cache;

namespace TransactionSample.Data
{
    public class DataObject : ICacheObject
    {
        public int Integer { get; set; }
        public string Str { get; set; }
        public DateTime Date { get; set; }

        public DataObject()
        {
            
        }

        public DataObject(int integer, string str, DateTime date)
        {
            Integer = integer;
            Str = str;
            Date = date;
        }

        public object Clone()
        {
            return new DataObject(Integer, Str, Date);
        }
    }
}
